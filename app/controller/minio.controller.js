import fs from 'fs';
import objectStorageUtil from '../utils/objectStorage.util';

const getFile = async (bucketName, fileName) => {
  const result = await objectStorageUtil
    .getObjectUrl(bucketName, fileName)
    .catch(error => ({ error: error.message }));
  return result;
};

const updateFile = async (bucketName, fileName, data) => {
  const result = await objectStorageUtil.createOrUpdateObject(bucketName, fileName, `${process.env.PWD}/${data.path}`, data);
  fs.unlinkSync(`${process.env.PWD}/${data.path}`);
  if (result.error) {
    return result;
  }
  return {};
};

const deleteFile = async (bucketName, fileName) => {
  const result = await objectStorageUtil.deleteObject(bucketName, fileName);
  return result;
};

export default {
  getFile,
  updateFile,
  deleteFile,
};
