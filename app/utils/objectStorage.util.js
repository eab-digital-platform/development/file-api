import * as minio from 'minio';

import getConfig from './config.util';

const connectObjectStorage = () => {
  const minioClient = new minio.Client({
    endPoint: getConfig('minio.endPoint'),
    port: getConfig('minio.port'),
    useSSL: getConfig('minio.useSSL'),
    accessKey: getConfig('minio.accessKey'),
    secretKey: getConfig('minio.secretKey'),
  });
    // For S3 in the future
    // var s3Client = new Minio.Client({
    //     endPoint:  's3.amazonaws.com',
    //     accessKey: 'YOUR-ACCESSKEYID',
    //     secretKey: 'YOUR-SECRETACCESSKEY'
    // })
  return minioClient;
};

const getObjectMeta = async (bucketName, objectKey) => {
  const meta = await connectObjectStorage()
    .statObject(bucketName, objectKey)
    .catch(error => ({ error }));
  return meta;
};

const getObjectUrl = async (bucketName, objectKey) => {
  const isBucketExists = await connectObjectStorage()
    .bucketExists(bucketName)
    .catch(error => ({ error }));
  console.log(`isBucketExists:${isBucketExists}`);
  if (isBucketExists) {
    const meta = await getObjectMeta(bucketName, objectKey);
    console.log('meta', meta);
    if (!meta.size) {
      return { error: 'object not exists.' };
    }
    const url = await connectObjectStorage()
      .presignedUrl('GET', bucketName, objectKey, getConfig('minio.objectExpirySeconds'))
      .catch(error => ({ error }));
    console.log(`url: ${url}`);
    if (url.error) {
      return { error: url.error };
    }
    return url;
  }
  return { error: 'Bucket not exists.' };
};

const deleteObject = async (bucketName, objectKey) => {
  const isBucketExists = await connectObjectStorage()
    .bucketExists(bucketName)
    .catch(error => ({ error }));
  if (isBucketExists) {
    const meta = await getObjectMeta(bucketName, objectKey);
    if (!meta.size) {
      return { error: 'object not exists.' };
    }
    const result = await connectObjectStorage()
      .removeObject(bucketName, objectKey)
      .catch(error => ({ error }));
    if (result && result.error) {
      return { error: result.error };
    }
    return result;
  }
  return { error: 'Bucket not exists.' };
};

const bucketExists = async bucketName => new Promise((resolve, reject) => {
  connectObjectStorage().bucketExists(bucketName, (err, result) => {
    if (err) {
      reject(err);
    } else {
      resolve(result);
    }
  });
});

const createOrUpdateObject = async (bucketName, objectKey, filePath, metaData) => {
  const result = await connectObjectStorage()
    .fPutObject(bucketName, objectKey, filePath, metaData)
    .catch(error => ({ error }));
  if (result.error) {
    return { error: result.error };
  }
  return {};
};

export default {
  connectObjectStorage,
  getObjectUrl,
  deleteObject,
  bucketExists,
  createOrUpdateObject,
};
