import express from 'express';
import multer from 'multer';
import {
  generalSuccessResponse, generalBadRequestResponse,
} from '../utils/response.util';
import minioController from '../controller/minio.controller';

const router = express.Router();

const upload = multer({ dest: 'uploads/' });

router.get('/:bucketName/:fileName', async (req, res) => {
  const result = await minioController.getFile(req.params.bucketName, req.params.fileName);
  if (result.error) {
    generalBadRequestResponse(res, result.error);
  } else {
    generalSuccessResponse(res, result);
  }
});

router.put('/:bucketName/:fileName', upload.single('file'), async (req, res) => {
  if (req.file) {
    const result = await minioController
      .updateFile(req.params.bucketName, req.params.fileName, req.file);
    generalSuccessResponse(res, result);
  } else {
    generalBadRequestResponse(res, { error: 'File not exists' });
  }
});

router.delete('/:bucketName/:fileName', async (req, res) => {
  const result = await minioController.deleteFile(req.params.bucketName, req.params.fileName);
  generalSuccessResponse(res, result);
});

export default router;
