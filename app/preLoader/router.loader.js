import { generalNotFoundResponse } from '../utils/response.util';
import testRouter from '../routes/test.route';
import minioRouter from '../routes/minio.router';

const loadRouterMiddleware = (app) => {
  app.use('/file-api/minio', minioRouter);
  app.use('/test', testRouter);
  // 404 handler
  app.use((req, res) => {
    generalNotFoundResponse(res);
  });
};

export default loadRouterMiddleware;
